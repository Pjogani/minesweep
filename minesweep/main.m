//
//  main.m
//  minesweep
//
//  Created by Pjo on 07/09/14.
//  Copyright (c) 2014 PRATEEKJOGANI. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PJoAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PJoAppDelegate class]));
    }
}
